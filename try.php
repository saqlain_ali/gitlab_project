<?php

$hookfile = 'payload.sh';

$logfile = 'payload.log';

$ref = 'refs/heads/master';
// THE ACTUAL SCRIPT
// -----------------
// You shouldn't edit beyond this point,
// unless you know what you're doing
// =====================================
function log_append($message, $time = null)
{
    global $logfile;
    $time = $time === null ? time() : $time;
    $date = date('Y-m-d H:i:s');
    $pre  = $date . ' (' . $_SERVER['REMOTE_ADDR'] . '): ';
    file_put_contents($logfile, $pre . $message . "\n", FILE_APPEND);
}
function exec_command($command)
{
    $output = array();
    exec($command, $output);
    log_append('EXEC: ' . $command);
    foreach ($output as $line) {
        log_append('SHELL: ' . $line);
    }
}

     function upload_payload($payload) {


        logMsg($payload);
        global $projects;

	$data = json_decode($payload);
	$slug = $data->repository->slug;
	$uri = $data->repository->absolute_url;

	$commit_data = array();
	foreach($data->commits as $commit) {
		$branch = $commit->branch;
                $valid=false;
                foreach($projects as $prj){                   
                    if($slug == $prj['git_slug']){                        
                        foreach($prj['branches'] as $brc){                            
                            if($brc==$branch){
                                $valid=true;
                            }                            
                        }                        
                    }                                        
                }
                if($valid==false){logMsg(' Ignoring this Commit '); continue;}
                logMsg(' Valid Commit Found ');
		$node = $commit->node;
		if(!array_key_exists($branch, $commit_data)) {
			$commit_data[$branch] = array();
		}
		foreach($commit->files as $file) {
			$commit_data[$branch][] = array($node, $file);
		}
	}
	$black_list = include 'blacklist.inc.php';
	$logMsg = '';
	foreach($commit_data as $branch=>$data) {

                $dir_path='../';
		foreach($data as $step) {
			list($node,$file) = $step;
			$path = $file->file;
			$is_ignored = false;
			foreach ($black_list['items'] as $item) {
				if(strpos($path, $item) !== false && array_search($path, $black_list['exceptions'])===false){
					$is_ignored = true;
					break;
				}
			}
			if($is_ignored){
				$logMsg .= logIt('Ignored '.$ftp_path.$path,false);
				continue;
			}
			if ($file->type=="removed") {
				unlink($dir_path.$path);
				$logMsg .= logIt('Removed '.$dir_path.$path,false);
			}else{
				//$url = "https://api.bitbucket.org/1.0/repositories".$uri."raw/".$node."/".$file->file;
                       $url = "https://gitlab.com/api/v4/projects/15521540/repository/files/".$uri."raw/".$node."/".$file->file;
				$dirname = dirname($path);
                                if(!file_exists($dir_path.$dirname)){
                                    if(mkdir($dir_path.$dirname)){
                                            $logMsg .= logIt('Created new directory '.$dirname,false);
					} else {
                                            $logMsg .= logIt('Error: failed to create new directory '.$dirname,false);
					}
                                }

				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_USERPWD, gzuncompress(base64_decode($bu)).":".gzuncompress(base64_decode($bp)));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

				$data = curl_exec($ch);

				curl_close($ch);

				file_put_contents($dir_path.$path, $data);

				$logMsg .= logIt('Uploaded: '.$dir_path.$path,false);
			}
		}
	}

	logMsg($logMsg);
}

 //curl --request GET --header 'PRIVATE-TOKEN: <your_access_token>' 'https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fmodels%2Fkey%2Erb/raw?ref=master'
$ch = curl_init();                                     // Curl Initialization
$url= "https://gitlab.com/api/v4/projects/15521540/repository/files/".$file->file."/raw?ref=master";

// $url = "https://api.bitbucket.org/1.0/repositories".$uri."raw/".$node."/".$file->file;

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);      //Curl SSL Verify peer FALSE
curl_setopt($ch, CURLOPT_URL, $url);                 //Curl URL 
$header = array();                                  //ARRAY FOR HEADER DATA
$header[] = 'PRIVATE-TOKEN: fsL3LGvssyo-W_PR7EpU'; //Header information-gitlab private token
curl_setopt($ch, CURLOPT_HEADER, $header);        //Curl Header  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);     //return the transfer as a string
$output = curl_exec($ch);                       // $output contains the output string
curl_close($ch);                               // close curl resource to free up system resources
file_put_contents('C:\\Bitnami\wampstack-7.1.8-0\apache2\htdocs\gitlab_project\payload.log',$output,FILE_APPEND);

// GitLab sends the json as raw post data
$input = file_get_contents("C:\\Bitnami\wampstack-7.1.8-0\apache2\htdocs\gitlab_project\payload.log");
$json  = json_decode($input);
if (!is_object($json) || empty($json->ref)) {
    log_append('Invalid push event data');
    die();
}
if (isset($ref))
{
    $_refs = (array) $ref;
    if ($ref !== '*' && !in_array($json->ref, $_refs)) {
        log_append('Ignoring ref ' . $json->ref);
        die();
    }
}
log_append('Launching shell hook script...');
exec_command('sh '.$hookfile);
log_append('Shell hook script finished');