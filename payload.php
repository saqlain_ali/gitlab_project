<?php
/**
 * GitLab Web Hook
 * See https://gitlab.com/kpobococ/gitlab-webhook
 *
 * This script should be placed within the web root of your desired deploy
 * location. The GitLab repository should then be configured to call it for the
 * "Push events" trigger via the Web Hooks settings page.
 *
 * Each time this script is called, it executes a hook shell script and logs all
 * output to the log file.
 *
 * This hook uses php's exec() function, so make sure it can be executed.
 * See http://php.net/manual/function.exec.php for more info
 */
// CONFIGURATION
// =============
/* Hook script location. The hook script is a simple shell script that executes
 * the actual git push. Make sure the script is either outside the web root or
 * inaccessible from the web
 *
 * This setting is REQUIRED  //mypushwebhook secret token 
 */
//$hookfile = '/path/to/the/script/to/execute.sh';
$hookfile = 'payload.sh';
/* Log file location. Log file has both this script's and shell script's output.
 * Make sure PHP can write to the location of the log file, otherwise no log
 * will be created!
 *
 * This setting is REQUIRED
 */
$logfile = 'payload.log';

/* Hook password. If set, this password should be passed as a GET parameter to
 * this script on every call, otherwise the hook won't be executed.
 *
 * This setting is RECOMMENDED
 */

/* $password = '12345ab';
   Ref name. This limits the hook to only execute the shell script if a push
 * event was generated for a certain ref (most commonly - a master branch).
 *
 * Can also be an array of refs:
 *
 *     $ref = array('refs/heads/master', 'refs/heads/develop');
 *
 * This setting does not support the actual refspec, so the refs should match
 * exactly.
 *
 * See http://git-scm.com/book/en/Git-Internals-The-Refspec for more info on
 * the subject of Refspec
 *
 * This setting is OPTIONAL
 */
$ref = 'refs/heads/master';
// THE ACTUAL SCRIPT
// -----------------
// You shouldn't edit beyond this point,
// unless you know what you're doing
// =====================================
function log_append($message, $time = null)
{
    global $logfile;
    $time = $time === null ? time() : $time;
    $date = date('Y-m-d H:i:s');
    $pre  = $date . ' (' . $_SERVER['REMOTE_ADDR'] . '): ';
    file_put_contents($logfile, $pre . $message . "\n", FILE_APPEND);
}
function exec_command($command)
{
    $output = array();
    exec($command, $output);
    log_append('EXEC: ' . $command);
    foreach ($output as $line) {
        log_append('SHELL: ' . $line);
    }
}

// GitLab sends the json as raw post data
$input = file_get_contents("php://input");
$json  = json_decode($input);
if (!is_object($json) || empty($json->ref)) {
    log_append('Invalid push event data');
    die();
}
if (isset($ref))
{
    $_refs = (array) $ref;
    if ($ref !== '*' && !in_array($json->ref, $_refs)) {
        log_append('Ignoring ref ' . $json->ref);
        die();
    }
}
//

//curl --request GET --header 'PRIVATE-TOKEN: <your_access_token>' 'https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fmodels%2Fkey%2Erb/raw?ref=master'



log_append('Launching shell hook script...');
log_append(print_r($json,true));

     function upload_payload($payload) {


        logMsg($payload);
        global $projects;
        //$json = str_replace("\\", "", trim(html_entity_decode($payload), "\""));

	//$json = stripslashes($payload);

        //logMsg($json);

	$data = json_decode($payload);
	$slug = $data->repository->slug;
	$uri = $data->repository->absolute_url;

	$commit_data = array();
	foreach($data->commits as $commit) {
		$branch = $commit->branch;
                $valid=false;
                foreach($projects as $prj){                   
                    if($slug == $prj['git_slug']){                        
                        foreach($prj['branches'] as $brc){                            
                            if($brc==$branch){
                                $valid=true;
                            }                            
                        }                        
                    }                                        
                }
                if($valid==false){logMsg(' Ignoring this Commit '); continue;}
                logMsg(' Valid Commit Found ');
		$node = $commit->node;
		if(!array_key_exists($branch, $commit_data)) {
			$commit_data[$branch] = array();
		}
		foreach($commit->files as $file) {
			$commit_data[$branch][] = array($node, $file);
		}
	}
	$black_list = include 'blacklist.inc.php';
	$logMsg = '';
	foreach($commit_data as $branch=>$data) {

                $dir_path='../';
		foreach($data as $step) {
			list($node,$file) = $step;
			$path = $file->file;
			$is_ignored = false;
			foreach ($black_list['items'] as $item) {
				if(strpos($path, $item) !== false && array_search($path, $black_list['exceptions'])===false){
					$is_ignored = true;
					break;
				}
			}
			if($is_ignored){
				$logMsg .= logIt('Ignored '.$ftp_path.$path,false);
				continue;
			}
			if ($file->type=="removed") {
				unlink($dir_path.$path);
				$logMsg .= logIt('Removed '.$dir_path.$path,false);
			}else{
				//$url = "https://api.bitbucket.org/1.0/repositories".$uri."raw/".$node."/".$file->file;
                       $url = "https://bitbucket.org".$uri."raw/".$node."/".$file->file;
				$dirname = dirname($path);
                                if(!file_exists($dir_path.$dirname)){
                                    if(mkdir($dir_path.$dirname)){
                                            $logMsg .= logIt('Created new directory '.$dirname,false);
					} else {
                                            $logMsg .= logIt('Error: failed to create new directory '.$dirname,false);
					}
                                }

				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_USERPWD, gzuncompress(base64_decode($bu)).":".gzuncompress(base64_decode($bp)));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

				$data = curl_exec($ch);

				curl_close($ch);

				file_put_contents($dir_path.$path, $data);

				$logMsg .= logIt('Uploaded: '.$dir_path.$path,false);
			}
		}
	}

	logMsg($logMsg);
}


log_append('Shell hook script finished');